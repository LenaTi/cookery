from django.db import models


class Topics(models.Model):
    topic_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Tweet(models.Model):
    topic = models.ForeignKey(Topics)
    text = models.CharField(max_length=140)
    p_date = models.DateTimeField()

