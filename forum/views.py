from datetime import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils import timezone
from forum.models import Tweet



def topics(request):
    return render(request, "forum/topics.html")

def com(request):
    return render(
        request, 
        "forum/com.html",
        {
            "user": request.user.username,
            "tweets": Tweet.objects.filter(user=request.user).order_by("-p_date")
        }
    )


def process(request):
    tweet = request.POST["tweet"]
    t = Tweet(
            text=tweet,
            p_date=timezone.now(),
            user=request.user
    )
    t.save()
    return HttpResponseRedirect(reverse("com"))

