from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^com/$', 'com', name='com'),
    url(r'^proc/$', 'process', name='process'),
)
