from django.conf.urls import patterns, include, url
from cookery import settings


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^forum/', include('forum.urls', namespace='forum')),
    url(r'^$', 'blog.views.index', name='index'),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^topics/$', 'forum.views.topics', name='topics'),
    url(r'^recipe/$', 'blog.views.recipe', name='recipe'),
    url(r'^rating/$', 'blog.views.rating', name='rating'),
    url(r'^sitemap/$', 'blog.views.sitemap', name='sitemap'),
    url(r'^siterules/$', 'blog.views.siterules', name='siterules'),
    url(r'^myblog/$', 'blog.views.myblog', name='myblog'),
    url(r'^blogs/$', 'blog.views.blogs', name='blogs'),
    url(r'^parlor/$', 'blog.views.parlor', name='parlor'),
    url(r'^process/$', 'blog.views.process', name='process'),
    url(r'^admin/', include(admin.site.urls)),
)
