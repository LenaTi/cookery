
from django.shortcuts import render
from blog.models import RecipeComment, Recipe
from datetime import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from blog.forms import RecipeForm
from django.shortcuts import render


def recipe(request):
    return render(
        request,
        "blog/recipe.html",
        {
            "user": "Lena",
            "comment": RecipeComment.objects.all().order_by("-p_date")
        }
    )


def process(request):
    comment = request.POST["comment"]
    t = RecipeComment(text_comment=comment,
                      p_date=datetime.now())
    t.save()
    return HttpResponseRedirect(reverse('recipe'))


def myblog(request):
    return render(request, "blog/myblog.html",
        {
            "user": "Lena",
            "recipe": Recipe.objects.all().order_by("-p_date")
        }
    )


def add_recipe(request):
    recipe = request.POST["recipe"]
    t = request.POST["t"]
    r = Recipe(name_r=t, text=recipe, p_date=datetime.now())
    r.save()
    return HttpResponseRedirect(reverse('myblog'))


def index(request):
    return render(request, "blog/index.html")



def rating(request):
    return render(request, "blog/rating.html")


def sitemap(request):
    return render(request, "blog/sitemap.html")


def siterules(request):
    return render(request, "blog/siterules.html")


def blogs(request):
    return render(request, "blog/blogs.html")



def parlor(request):
    return render(request, "blog/parlor.html")


def add(request):

       if request.method == 'POST':
           form = RecipeForm(request.POST)
           if  form.is_valid():
               return render(request,'siterules', {'form': form})
       else:
           form = RecipeForm()
       return render(request,'siterules', {'form': form})