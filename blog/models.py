from django.db import models
from django.contrib.auth.models import User

class RecipeComment(models.Model):
    text_comment = models.CharField(max_length=150)
    p_date = models.DateTimeField()


class Recipe(models.Model):
    name_r = models.CharField(max_length=30,blank=True)
    text = models.TextField(blank=True)
    p_date = models.DateTimeField()
