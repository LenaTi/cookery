from django.conf.urls import url, patterns


urlpatterns = patterns('',
    url(r'^add_recipe/$', 'blog.views.add_recipe', name='add_recipe'),
    url(r'^add/$', 'blog.views.add', name='add'),
    )