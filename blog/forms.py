from django.forms import ModelForm, Textarea
from blog.models import Recipe

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = ['name_r', 'text']