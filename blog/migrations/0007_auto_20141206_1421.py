# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20141206_1205'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='user',
        ),
        migrations.DeleteModel(
            name='Profile',
        ),
        migrations.RemoveField(
            model_name='recipe',
            name='user',
        ),
        migrations.AlterField(
            model_name='recipe',
            name='name_r',
            field=models.CharField(max_length=30, blank=True),
        ),
    ]
